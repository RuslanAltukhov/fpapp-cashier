class CategoriesFilterAdapter {
    constructor(app, container) {
        this._container = container;
        this._app = app;
        this._data = [];
        this.setData([]);
    }

    static groupContainerTemplate() {
        return '<optgroup></optgroup>';
    }

    static groupContainerItemTemplate() {
        return '<option selected></option>';
    }

    setData(data) {
        this._data = data;
        return this;
    }

    renderGroupOptions(groupContainer, groupItems) {
        groupItems.forEach(function (groupItem) {
            var groupCategoryItem = $$(CategoriesFilterAdapter.groupContainerItemTemplate());
            groupCategoryItem.text(groupItem.title);
            groupCategoryItem.val(groupItem.id);
            groupContainer.append(groupCategoryItem);
        });
    }

    render() {
        var that = this;
        console.log("render categories");
        this._data.forEach(function (group) {
            var groupContainer = $$(CategoriesFilterAdapter.groupContainerTemplate());
            $$(groupContainer).attr("label", group.group_name);
            that.renderGroupOptions($$(groupContainer), group.categories);
            $$(that._container).append($$(groupContainer));
        });
    }
}