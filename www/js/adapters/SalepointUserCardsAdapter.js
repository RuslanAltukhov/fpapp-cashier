class SalepointUserCardsAdapter {
    constructor(app, container) {
        this._container = container;
        this._app = app;
        this._selected = [];
        this.setData([]);
    }

    getSelectedItems() {
        return this._selected;
    }

    addSelectedItem(actionId, that) {
        console.log("add selected item fort action id:", actionId);
        for (var pos = 0; pos < that._selected.length; pos++) {
            if (that._selected[pos].actionId == actionId) {
                that._selected[pos].count++;
                return;
            }
        }
        that._selected.push({actionId: actionId, count: 1});
    }

    removeSelectedItem(actionId, that) {
        for (var pos = 0; pos < that._selected.length; pos++) {
            if (that._selected[pos].actionId == actionId && that._selected[pos].count > 0) {
                that._selected[pos].count--;
                break;
            }
        }
    }

    static getTemplate() {
        return $$('  <div class="card card-offer">\n' +
            '                <div class="card-bg" style="background-color: #f1f1f1; color: #22aa68">\n' +
            '                    <div class="card-header">\n' +
            '                        <div class="company-logo"\n' +
            '                             style="background-image: url(\'https://semart.ru/demo/fp/like.png\')"></div>\n' +
            '                        <div class="company-offer">Каждый шестой стакан кофе в подарок!</div>\n' +
            '                    </div>\n' +
            '                    <div class="card-content card-content-padding fishki">\n' +
            '                        <ol class="row no-gap card_promo-fishki">\n' +
            '                        </ol>\n' +
            '                    </div>\n' +
            '                    <div class="card-footer" style="background-color: #22aa68; color: #f1f1f1;">\n' +
            '                        <span class="card-company-name">COFFEE LIKE</span>\n' +
            '                        <span class="card-expires">бессрочно</span>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '                <div class="offer-history custom-accordion">\n' +
            '                    <div class="accordion-item">\n' +
            '                        <div class="accordion-item-toggle"><i class="material-icons is-closed">expand_more</i> <i\n' +
            '                                class="material-icons is-open">expand_less</i></div>\n' +
            '                        <div class="accordion-item-content">\n' +
            '                            <div class="timeline">\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '\n' +
            '\n' +
            '                </div>\n' +
            '            </div>');
    }

    static getBuyLogItemTeamplate() {
        return $$('  <div class="timeline-item">\n' +
            '                                    <!--Дата и вермя покупки-->\n' +
            '                                    <div class="timeline-item-date"></div>\n' +
            '                                    <!-- Если получена фишка, ставим класс .fish, Если получена плюшка, ставим класс .plush-->\n' +
            '                                    <div class="timeline-item-divider  fish"></div>\n' +
            '                                    <div class="timeline-item-content">\n' +
            '                                        <!--Название места ТТ-->\n' +
            '                                        <div class="timeline-item-title">ТРЦ «Тандем»</div>\n' +
            '                                        <!--Адрес ТТ-->\n' +
            '                                        <div class="timeline-item-subtitle">Казань, пр-т Ибрагимова, д.56</div>\n' +
            '                                    </div>\n' +
            '                                </div>');
    }

    static getTipTeamplate() {
        return $$('<li class="col" style="color:#fff; fill: #222; stroke: #22aa68;">\n' +
            '                                <svg version="1.1" viewBox="0 0 500 500" preserveAspectRatio="xMinYMin meet"\n' +
            '                                     class="svg-content">\n' +
            '                                    <circle stroke-width="20" stroke-miterlimit="20" cx="250" cy="250" r="240"/>\n' +
            '                                </svg>\n' +
            '                            </li>');
    }


    setData(data) {
        this._data = data;
        return this;
    }

    addItems(data) {
        this._data = this._data.concat(data);
        return this;
    }

    addContentToLogItem(el, data) {
        var date = new Date(data.created_at);
        $$(el).find(".timeline-item-date").html(SalepointUserCardsAdapter.formatDate(date));
        $$(el).find(".timeline-item-title").text(data.salePoint.name);
        $$(el).find(".timeline-item-subtitle").text(data.salePoint.address);
    }

    renderSalesLog(container, data) {
        var that = this;
        data.forEach(function (salesLogItem) {
            var tmpl = SalepointUserCardsAdapter.getBuyLogItemTeamplate();
            that.addContentToLogItem(tmpl, salesLogItem);
            container.append(tmpl);
        });
    }

    addContentToItem(el, data) {
        var actionEndDate = new Date(data.end_at);
        $$(el).find('.card-company-name').text(data.organization.name);
        // $$(el).find('.card-expires').text(data.end_at ? "истекает через: " + this.getExpireDays(actionEndDate, new Date()) + " дн" : "бессрочна");
        $$(el).find('.company-offer').text(data.description);
        $$(el).find(".card-footer").css({
            "background": data.style.footer_bg_color,
            "color": data.style.footer_text_color
        });
        $$(el).find(".card-bg").css({
            "background": data.style.action_bg_color,
            "color": data.style.action_text_color
        });
        $$(el).find('.company-logo').css("background-image", "url(" + data.style.logo_url + ")");


        return el;
    }

    clearList() {
        console.log("clear list");
        $$(this._container).empty();
    }


    static formatDate(date) {
        return date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear() + "<small> " + date.getHours() + ":" + date.getMinutes() + "</small>";
    }

    itemClickListener(el, actionId, that) {
        var isChecked = el.hasClass("active");

        if (isChecked) {
            el.removeClass("active");
            that.removeSelectedItem(actionId, that);
            return;
        }
        that.addSelectedItem(actionId, that);
        el.addClass("active");
    }

    renderTips(container, data) {
        var that = this;
        var current_lap_boughts = data.currentLapPurchases.length;
        for (var pos = 0; pos < data.need_to_buy; pos++) {
            var tmpl = SalepointUserCardsAdapter.getTipTeamplate();
            if (pos + 1 <= current_lap_boughts) {
                $$(tmpl).addClass("checked");
                tmpl.css({
                    "stroke": data.style.active_item_border_color,
                    "fill": data.style.active_item_bg_color,
                    "color": data.style.active_item_text_color
                });
            } else { //еще не купленные
                tmpl.css({
                    "stroke": data.style.item_border_color,
                    "fill": data.style.item_bg_color,
                    "color": data.style.item_text_color
                });
                tmpl.click(function () {
                    that.itemClickListener($$(this), data.id, that);
                })
            }
            container.append(tmpl);
        }
        var last = SalepointUserCardsAdapter.getTipTeamplate();
        last.css({
            "stroke": data.style.last_item_border_color,
            "fill": data.style.last_item_bg_color,
            "color": data.style.last_item_text_color
        });
        last.click(function () {
            that.itemClickListener($$(this), data.id, that);
        });
        container.append(last);
    }

    render() {

        var that = this;
        this._data.forEach(function (item) {
            var template = SalepointUserCardsAdapter.getTemplate();
            var el = that.addContentToItem($$(template), item);
            that.renderTips($$(el).find("ol.card_promo-fishki"), item);
            that.renderSalesLog(el.find(".timeline"), item.currentLapPurchases)
            $$(that._container).append(el);
        });
    }
}