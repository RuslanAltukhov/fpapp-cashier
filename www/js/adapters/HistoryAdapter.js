class HistoryAdapter {
    constructor(app, container) {
        this._container = container;
        this._app = app;
        this.setData([]);
    }


    static getNoDataTemplate() {
        return ' <div class="block-title">НЕТ ДАННЫХ</div>';
    }

    static getSaleItemTemplate() {
        return '<div class="timeline-item-inner give-plus">\n' +
            '                        <div class="timeline-item-time">9:10</div>\n' +
            '                        <div class="timeline-item-title">Название акции</div>\n' +
            '                        <!--<div class="timeline-item-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit</div>-->\n' +
            '                        <a href="/offer-accept/" class="timeline-item-subtitle"><span>Клиент:</span> <strong>Имя / ID</strong></a>\n' +
            '                    </div>';
    }

    static getBonusItemTemplate() {
        return '<div class="timeline-item-inner give-bonus">\n' +
            '                        <div class="timeline-item-time">9:00</div>\n' +
            '                        <div class="timeline-item-title">Название акции</div>\n' +
            '                        <a href="/offer-accept/" class="timeline-item-subtitle"><span>Реферал:</span> <strong>Имя / ID</strong></a>\n' +
            '                    </div>';
    }

    setData(data) {
        this._data = data;
        return this;
    }

    addItems(data) {
        this._data = this._data.concat(data);
        return this;
    }

    clearList() {
        console.log("clear list");
        this._container.find(".timeline-item-content").empty();
    }

    renderHeader(data) {
        console.log(data);
        var monthRu = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"];
        var date = new Date(data.date);
        this._container.find(".plus").text(data.salesCount);
        this._container.find(".day").text(date.getDate());
        this._container.find(".month").text(monthRu[date.getMonth()]);
        this._container.find(".bonus").text(data.bonusCount);
    }

    static formatNumber(number) {
        if (number < 10) {
            return "0" + number;
        }
        return number;
    }

    addContentToItem(el, data) {
        var date = new Date(data.created_at);
        el.find(".timeline-item-title").text(data.action.name);
        el.find(".timeline-item-subtitle strong").text(data.user.name);
        el.find(".timeline-item-subtitle span").text(data.user.is_referal == 1 ? "Реферал:" : "Клиент:");
        el.find(".timeline-item-time").text(HistoryAdapter.formatNumber(date.getHours()) + ":" + HistoryAdapter.formatNumber(date.getMinutes()));
        var that = this;
        el.click(function () {
            that._app.views.main.router.navigate("/offer-accept/?uid=" + data.user.id);
        });
        return el;
    }


    render() {
        this.clearList();
        var that = this;
        if (this._data.length == 0) {
            $$(that._container.find(".timeline-item-content")).append(HistoryAdapter.getNoDataTemplate());

        }
        this._data.forEach(function (item) {
            var template = item.type == 0 ? HistoryAdapter.getSaleItemTemplate() : HistoryAdapter.getBonusItemTemplate();
            var el = that.addContentToItem($$(template), item);
            that._container.find(".timeline-item-content").append(el);
        });
    }
}