class SideMenu {
    constructor() {
        this.initUI();
    }

    initUI() {
        var that = this;
        this._loginBtn = $$("#go_to_login");
        this._logoutBtn = $$("#logout_btn").click(function () {
            that.logoutBtnClick();
        });
    }

    changeStateToAuthorized() {
        this._logoutBtn.show();
        this._loginBtn.hide();
    }

    changeStateToNotAuthorized() {
        this._logoutBtn.hide();
        this._loginBtn.show();
    }

    logoutBtnClick() {
        console.log("выход");
        AuthToken.clearAuthData();
        User.clearUserInStorage();
        this.changeStateToNotAuthorized();
        onLoggedOut();
    }
}