class AllActionsMapFragment {
    constructor(config, actionsModel, appObject) {
        this._app = appObject;
        this._actionsModel = actionsModel;
        this._map = null;
    }

    initMap() {

        if (this._map) {
            return;
        }

        $$("#map").css({'height': this.getPageHeight() + 'px'});
        var interactions = ol.interaction.defaults({altShiftDragRotate: false, pinchRotate: false});

        this._map = new ol.Map({
            interactions: interactions,
            target: 'map',
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM()
                })
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([-73.1234, 45.678]),
                zoom: 10
            })
        });
    }

    getPageHeight() {
        return $$('.page-content').height();
    }

    showPopoverOnMap() {
        var dynamicPopover = this._app.popover.create({
            targetX: evt.pixel[0],
            targetY: evt.pixel[1],
            content: '<div class="popover company-on-map">' +
            '<div class="popover-inner">' +
            '<div class="block map-item">' +
            '<a href="#" class="link color-white popover-close"><i class="material-icons">close</i></a>' +
            '<div class="map-item--logo">' +
            '<img src="../img/temp/burger.png">' +
            '</div>' +
            '<div class="map-item--info">' +
            '<div class="map-item--title">Название компании</div>' +
            '<div class="map-item--adress">Адрес компании</div>' +
            '<div class="map-item--description">Здесь примечание, которое компания в админке написала о совей ТТ</div>' +
            '</div>' +
            '<div class="map-item--footer"><button class="button button-small">Все предложения</button></div>' +
            '</div>' +
            '</div>' +
            '</div>',
            // Events
            on: {
                open: function (popover) {
                    console.log('Popover open');
                },
                opened: function (popover) {
                    console.log('Popover opened');
                },
            }
        });
        dynamicPopover.open();
    }

    showActionsOnMap() {

        var iconFeatures = [];

        var iconFeature = new ol.Feature({
            geometry: new ol.geom.Point(ol.proj.transform([-72.0704, 46.678], 'EPSG:4326',
                'EPSG:3857')),
            name: 'Null Island',
            population: 4000,
            rainfall: 500
        });

        iconFeatures.push(iconFeature);

        var vectorSource = new ol.source.Vector({
            features: iconFeatures //add an array of features
        });

        var iconStyle = new ol.style.Style({
            image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
                anchor: [0.5, 46],
                anchorXUnits: 'pixels',
                anchorYUnits: 'pixels',
                opacity: 1,
                src: 'img/marker.png'
            }))
        });


        var vectorLayer = new ol.layer.Vector({
            source: vectorSource,
            style: iconStyle
        });
        this._map.addLayer(vectorLayer);
        var that = this;

        /*this._map.on('click', function (evt) {
            var feature = map.forEachFeatureAtPixel(evt.pixel,
                function (feature, layer) {
                    return feature;
                });
            if (feature) {
                that.showPopoverOnMap();
            }
        });*/
    }

}