class BarcodePopup {
    constructor(app) {
        this._app = app;
        this.loadImageFromStorage();
    }

    loadImageFromStorage() {
        var user = User.getFromStorage();
        if (!user) {
            return;
        }
        this._barcode = user.barcode_base64;
    }

    clearBarcodeImage() {
        this._barcode = null;
    }

    isBarcodeLoaded() {
        return typeof this._barcode == "string";
    }

    setBarcodeImage() {

        this.barcodeImg.attr("src", this._barcode);
    }

    onBarcodeBtnClick() {
        if (!this.isBarcodeLoaded()) {
           // this._app.views.main.router.navigate("/login/");
        }
    }

    onPageLoaded() {
        var that = this;
        this._barcodeBtn = $$(".barcode_btn").click(function () {
            that.onBarcodeBtnClick();
        });
        this.barcodeImg = $$(".barcode");
        this.setBarcodeImage();
    }

}