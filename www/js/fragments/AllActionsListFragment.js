class AllActionsListFragment {
    constructor(config, actionsModel, appObject) {
        this._app = appObject;
        this._actionsModel = actionsModel;
        this._actionsListAdapter = new HistoryAdapter(this._app, $$("#all_actions_container"));
        this._categoriesListAdapter = new CategoriesFilterAdapter(appObject, $$("#categories_filter"));
        this.initUI();
    }

    getSearchParams() {
        return this._params || {};
    }

    clearSearchParams() {
        this._params = {};
    }

    addSearchParam(key, value) {
        if (!this._params) {
            this._params = {};
        }
        this._params[key] = value;
    }

    initPullToRefresh() {
        var ptr = $$("#all_actions_page");
        var that = this;
        ptr.on("ptr:refresh", function () {
            that.showActions();
        });
    }

    initFilter() {
        var that = this;

        var categoriesLoaded = function (data) {
            that._categoriesListAdapter.setData(data);
            that._categoriesListAdapter.render();
            that.initSmartSelect();
        }
        this._actionsModel.loadCategories({}, categoriesLoaded);
    }

    initSmartSelect() {
        var that = this;
        var smartSelect = this._app.smartSelect.create({
            el: ".smart-select",
            on: {
                close: function (sm) {
                    that.addSearchParam("category", sm.getValue());
                    that.showActions()
                }
            }
        });
    }

    initSearchBar() {
        var that = this;
        var currentTime = new Date().getTime();
        var onSearch = function (sb, query, previousQuery) {
            currentTime = new Date().getTime();
            that.addSearchParam("queryStr", query);
            that.showActions();
        }

        console.log("init searchbar");
        this._searchbar = this._app.searchbar.create({
            el: '.searchbar',
            backdrop: false,
            customSearch: true,
            on: {
                search: onSearch
            }
        });
    }

    initUI() {
        this.initSearchBar();
        this.initFilter();
        this.initBarCode();
        this.initPullToRefresh();
    }

    initBarCode() {
        var data = User.getFromStorage();
        console.log(data.barcode_base64);
        if (data.barcode_base64) {
            $$("#barcode").attr("src", data.barcode_base64);
        }
    }

    showActions() {
        var that = this;
        var actionsLoaded = function (data) {
            console.log("actions loaded from server");
            that._app.ptr.get("#all_actions_page").done();
            that._actionsListAdapter.clearList();
            that._actionsListAdapter.setData(data);
            that._actionsListAdapter.render();

        }
        this._actionsModel.loadAllActions(this.getSearchParams(), actionsLoaded, null);
    }
}