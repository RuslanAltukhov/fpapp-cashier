class User {
    constructor(appObject, config) {
        this._appObject = appObject;
        this._config = config;
    }

    addToken(token) {
        this._token = token;
    }

    removeToken() {
        delete this._token;
    }

    get name() {
        return this._name || "";
    }

    get phone() {
        return this._phone || "";
    }

    get barCode() {
        return this._barcode_base64 || "";
    }

    isTokenExist() {
        return this._token != undefined;
    }

    static getStorageFields() {
        return ["name", "id", "user_id"];
    }

    static getStorageKey() {
        return "currentUserProfile";
    }

    static saveToStorage(data) {

        var fields = {};
        User.getStorageFields().forEach(function (storageKey) {
            fields[storageKey] = data[storageKey] || "";
        })

        console.log("saving user data to storage:", fields);
        localStorage.setItem(User.getStorageKey(), JSON.stringify(fields));
    }

    static clearUserInStorage() {
        localStorage.removeItem(User.getStorageKey());
    }

    static getFromStorage() {
        var localStorage = window.localStorage;
        return JSON.parse(localStorage.getItem(User.getStorageKey()))
    }

    loginUser(loginAndPassword, onSuccess, onFailure) {
        this._appObject.request.post(this._config.getFullUrl(this._config.urlList.login), loginAndPassword, onSuccess, onFailure, 'json');
    }


    loadSalePoints(data, onSuccess, onFailure) {
        if (!this.isTokenExist()) {
            onFailure({'text': 'Вам необходимо авторизоваться в приложении'});
        }
        this._appObject.request.get(this._config.getFullUrlWithSessid(this._config.urlList.salepoints, this._token), data, function (data) {
            onSuccess(data);
        }, onFailure, 'json');
    }

    chooseSalepoint(data, onSuccess, onFailure) {
        if (!this.isTokenExist()) {
            onFailure({'text': 'Вам необходимо авторизоваться в приложении'});
        }
        this._appObject.request.post(this._config.getFullUrlWithSessid(this._config.urlList.chooseSalepoint, this._token), data, onSuccess, onFailure, 'json');
    }

    loadUserCards(params, onSuccess, onFailure) {
        if (!this.isTokenExist()) {
            onFailure({'text': 'Вам необходимо авторизоваться в приложении'});
        }
        params['sessid'] = this._token;
        this._appObject.request.get(this._config.getFullUrlWithSessid(this._config.urlList.userCards, this._token), params, onSuccess, onFailure, 'json');
    }

    sendStamps(params, onSuccess, onFailure) {
        if (!this.isTokenExist()) {
            onFailure({'text': 'Вам необходимо авторизоваться в приложении'});
        }
        params['sessid'] = this._token;
        this._appObject.request.postJSON(this._config.getFullUrlWithSessid(this._config.urlList.stamps, this._token), params, onSuccess, onFailure, 'json');
    }

    loadHistory(params, onSuccess, onFailure) {
        if (!this.isTokenExist()) {
            onFailure({'text': 'Вам необходимо авторизоваться в приложении'});
        }
        params['sessid'] = this._token;
        this._appObject.request.get(this._config.getFullUrlWithSessid(this._config.urlList.history, this._token), params, onSuccess, onFailure, 'json');
    }

    sendMessage(params, onSuccess, onFailure) {
        if (!this.isTokenExist()) {
            onFailure({'text': 'Вам необходимо авторизоваться в приложении'});
        }
        params['sessid'] = this._token;
        this._appObject.request.post(this._config.getFullUrlWithSessid(this._config.urlList.supportMessage, this._token), params, onSuccess, onFailure, 'json');
    }

}