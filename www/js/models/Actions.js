class Actions {
    constructor(appObject, config) {
        this._appObject = appObject;
        this._config = config;
    }

    addUserToken(token) {

        this._token = token;
        console.log("add token:", this._token);
    }

    removeToken() {
        delete this._token;
    }

    isUserTokenExist() {
        return this._token != undefined;
    }

    loadAllActions(params, onSuccess, onFailure) {
        console.log(params);
        if (this.isUserTokenExist()) {
            params['sessid'] = this._token;
        }
        this._appObject.request.get(this._config.getFullUrl(this._config.urlList.allActionsUrl), params, function (data) {
            console.log(data);
            onSuccess(data);
        }, onFailure, 'json');
    }

    loadUserActions(params, onSuccess, onFailure) {
        console.log(this._token);
        params = params || {};
        console.log(params);
        if (this.isUserTokenExist()) {
            params['sessid'] = this._token;
        }
        console.log(params);
        this._appObject.request.get(this._config.getFullUrl(this._config.urlList.userActionsUrl), params, function (data) {
            console.log(data);
            onSuccess(data);
        }, onFailure, 'json');
    }

    loadCategories(params, onSuccess, onFailure) {

        this._appObject.request.get(this._config.getFullUrl(this._config.urlList.categories), params, function (data) {
            console.log(data);
            onSuccess(data);
        }, onFailure, 'json');
    }
}