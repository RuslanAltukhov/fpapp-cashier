class AuthToken {

    constructor(jsoData, config, appObject) {
        if (!jsoData.token) {
            throw new Error("'token' param is expected but not exist");
        }

        this._config = config;
        this._app = appObject;
        this.setSalePointId(jsoData.salepointId);
        this._token = jsoData.token;
    }

    static createNew(token, config, appObject) {
        return new AuthToken({token: token}, config, appObject);
    }

    getAsJson() {
        console.log({token: this._token, salepointId: this._salepointId});
        return {token: this._token, salepointId: this._salepointId};
    }

    getToken() {
        return this._token;
    }

    static getLocalStorageKey() {
        return 'authToken';
    }

    save() {
        var storage = window.localStorage;
        storage.setItem(AuthToken.getLocalStorageKey(), JSON.stringify(this.getAsJson()));
    }

    static getFromStorage(config, appObject) {
        var storage = window.localStorage;
        var data = storage.getItem(AuthToken.getLocalStorageKey());
        return data ? new AuthToken(JSON.parse(data), config, appObject) : null;
    }

    static clearAuthData() {
        window.localStorage.removeItem(AuthToken.getLocalStorageKey());
    }

    isSalepointChoosed() {
        return this._salepointId != undefined && this._salepointId != null;
    }

    setSalePointId(id) {
        this._salepointId = id;

    }

    getSalePointId() {
        return this._salepointId;
    }

    isActive(onSuccess, onFailure) {
        console.log(this._config.getFullUrl(this._config.urlList.checkAuthToken));
        this._app.request.get(this._config.getFullUrl(this._config.urlList.checkAuthToken), {sessid: this._token}, function (data) {
            if (data.code == 0) { // login success
                onSuccess(data);
                return;
            }
            onFailure(data);
        }, null, 'json');
    }
}