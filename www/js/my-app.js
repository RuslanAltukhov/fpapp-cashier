// Initialize app
var $$ = Dom7;
var app = new Framework7({
    root: '#app',
    theme: 'md',
    panel: {
        swipe: 'left',

    },
    popup: {
        backdrop: false,
        swipeToClose: true,
    },
    touch: {
        // Enable fast clicks
        fastClicks: true,
    },
    navbar: {
        hideOnPageScroll: true,
        iosCenterTitle: true,
    },
    routes: [

        {
            name: "login",
            path: '/login/',
            url: 'login.html',
        },
        {
            name: "history",
            path: '/history/',
            url: 'history.html',
        },
        {
            name: "offer-accept",
            path: '/offer-accept/',
            url: 'offer-accept.html',
        },
        {
            name: "support",
            path: '/support/',
            url: 'support.html',
        },
        {
            name: "profile",
            path: '/profile/',
            url: 'profile.html',
        },
        {
            name: "code-reader",
            path: '/code_reader/',
            url: 'code_reader.html',
        }
    ]
});

window.onerror = function (e) {
    app.dialog.alert(e);
};
var loginPage;
var codeReaderPage;
var historyPage;
var supportPage;
var sideMenu = new SideMenu();
var profilePage;
var offerAcceptPage;
var authToken = AuthToken.getFromStorage(config, app);
var actionsModel = new Actions(app, config);
var userModel = new User(app, config);
var userActionsPage;
// If we need to use custom DOM library, let's save it to $$ variable:


// Add view
var mainView = app.views.create('.view-main', {
    // Because we want to use dynamic navbar, we need to enable it for this view:
    dynamicNavbar: true,
    url: '/login/'
});
$$('.left-menu a').on('click', function () {
    app.panel.close();

});

var onAuthorized = function (authToken) {
    console.log(authToken.getToken());
    userModel.addToken(authToken.getToken());
    //sideMenu.changeStateToAuthorized();
    //barcodePopup.loadImageFromStorage();
}

var onLoggedOut = function () {
    // barcodePopup.clearBarcodeImage();
    userModel.removeToken();
    //actionsModel.removeToken();
}


$$(document).on('deviceready', function () {
    app.preloader.show();
    if (!authToken) {
        app.preloader.hide();
        console.log('auth token is not exist');
        return;
    }
    var onSuccess = function (data) {
        console.log("auth token is checked");
        onAuthorized(authToken);
        app.preloader.hide();

        if (authToken.isSalepointChoosed()) {
            app.views.main.router.navigate("/history/");
            return;
        }
        app.views.main.router.navigate("/profile/");

    }
    var onFailure = function (data) {
        app.dialog.alert(1000);
        console.log("auth token is invalid");
        app.preloader.hide();
    }
    setTimeout(function () {
        authToken.isActive(onSuccess, onFailure);
    }, 1000);

});


// Option 2. Using one 'pageInit' event handler for all pages:
$$(document).on('page:init', '.page[data-name="login"]', function (e, page) {
    var action = page.route.query.action || "";
    loginPage = new LoginPage(config, app, new User(app, config));
    if (action == "logout") {
        AuthToken.clearAuthData();
        onLoggedOut();
    }
});
$$(document).on('page:init', '.page[data-name="profile"]', function (e) {
    profilePage = new ProfilePage(app, userModel, authToken);
});
$$(document).on('page:init', '.page[data-name="support"]', function (e) {
    supportPage = new SupportPageController(app, userModel);
});
$$(document).on('page:init', '.page[data-name="history"]', function (e) {
    historyPage = new HistoryPage(app, userModel);
});
$$(document).on('page:afterin', '.page[data-name="history"]', function (e) {
    historyPage.loadHistory();
});
$$(document).on('page:init', '.page[data-name="code-reader"]', function (e) {
    codeReaderPage = new CodeReaderPage(config, app, userModel);
});
$$(document).on('page:init', '.page[data-name="offer-accept"]', function (e, page) {
    var uid = page.route.query.uid || "";
    offerAcceptPage = new OfferAcceptPage(uid, app, userModel);
});
/*$$(document).on('page:afterin', function (e) {
    console.log("page afterin event");
    barcodePopup.onPageLoaded();
});
$$(document).on('page:afterin', '.page[data-name="all_actions"]', function (e) {
    app.panel.disableSwipe("left");
});
$$(document).on('page:afterout', '.page[data-name="all_actions"]', function (e) {
    app.panel.enableSwipe("left");
});
$$(document).on('page:init', '.page[data-name="all_actions"]', function (e) {
    allActionsPage = new AllActionsPage(config, app, actionsModel);
    allActionsPage.showActionsOnListFragment();

});
$$(document).on('page:afterin', '.page[data-name="my_actions"]', function (e) {
    userActionsPage.showActions();
});


$$(document).on('page:init', '.page[data-name="my_actions"]', function (e) {
    userActionsPage = new UserActionsPage(config, app, actionsModel);
});
$$(document).on('page:afterin', '.page[data-name="profile"]', function (e) {
    profilePage.showCurrentProfile();
});*/
