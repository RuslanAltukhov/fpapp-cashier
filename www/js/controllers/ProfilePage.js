class ProfilePage {
    constructor(app, userModel, authToken) {
        this._app = app;
        this._authToken = authToken;
        this._userModel = userModel;
        this.initUI();
    }

    initUI() {
        this._salepointsList = $$("#salepoints");
        var that = this;
        $$("#your-id").text(User.getFromStorage().id);
        $$("#chose_btn").click(function () {
            that.chooseSalepoint(that);
        });
        this.loadSalepoints();
    }

    fillSalepointsList(data) {
        var that = this;
        this._salepointsList.empty();
        data.forEach(function (item) {
            that._salepointsList.append("<option value='" + item.id + "'>" + item.name + "</option>")
        });
        var smartSelect = app.smartSelect.get('#smartselect');
        if (this._authToken.getSalePointId() != undefined) {
            smartSelect.setValue(this._authToken.getSalePointId());
            return;
        }
        if (data[0] != undefined) {
            smartSelect.setValue(data[0].id);
        }
    }

    chooseSalepoint(that) {
        var that = this;
        var onSuccess = function (data) {
            authToken.setSalePointId(data.salePoint);
            authToken.save();
            that._app.views.main.router.navigate("/history/");
        };
        var onFailure = function (data) {
            console.log(data);
        }
        this._userModel.chooseSalepoint({salepointId: that._salepointsList.val()}, onSuccess, onFailure);
    }

    initSmartSelect() {
        var that = this;
        var smartSelect = this._app.smartSelect.create({
            el: "#smartselect",
        });
    }

    loadSalepoints() {
        var that = this;
        var dataLaded = function (data) {
            that.fillSalepointsList(data);
            that.initSmartSelect();
        }
        var dataLoadError = function (data) {
            console.log(data);
        }
        this._userModel.loadSalePoints({}, dataLaded, dataLoadError);
    }

}