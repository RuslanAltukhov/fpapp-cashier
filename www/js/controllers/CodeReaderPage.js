class CodeReaderPage {
    constructor(config, appObject) {
        this._app = appObject;
        this.initUI();
    }

    initUI() {
        var that = this;
        this._codeInput = $$("#code-input");
        $$("#search-user-btn").click(function () {
            that.gotToUserActions(that._codeInput.val());
        })
        $$("#scan_btn").click(function () {
            that.scanBarcode();
        })
    }

    scanBarcode() {
        var that = this;
        cordova.plugins.barcodeScanner.scan(
            function (result) {
                if (result.cancelled) {
                    return;
                }

                that.gotToUserActions(result.text);

            },
            function (error) {
                that._app.dialog.alert("Ошибка сканирования кода, попробуйте еще раз", "Внимание!");
            }
        );
    }

    gotToUserActions(uid) {
        if (uid.length == 0 || isNaN(uid)) {
            this._app.dialog.alert("Укажите код пользователя", "Ошибка");
            return;
        }

        this._app.views.main.router.navigate("/offer-accept/?uid=" + uid);
    }

}