class HistoryPage {
    constructor(app, userModel) {
        this._app = app;
        this._userModel = userModel;
        this._adapters = [];
        this.initUI();
    }

    initUI() {
        //this.loadHistory();
    }

    loadHistory() {
        var date = new Date();
        var that = this;
        var onSuccess = function (data) {
            that.renderData(data);
        };
        var onError = function (data) {
            console.log(data);
        }
        this._userModel.loadHistory({date: date.getUTCFullYear() + "-" + (date.getUTCMonth() + 1) + "-" + date.getUTCDate()}, onSuccess, onError);
    }

    renderData(data) {
        var that = this;
        var containers = $$(".timeline-item");
        var pos = 0;
        data.forEach(function (salesForDate) {
            console.log(salesForDate);
            var adapter = new HistoryAdapter(that._app, containers.eq(pos++));
            adapter.setData(salesForDate.items);
            adapter.renderHeader(salesForDate);
            adapter.render();
        });
    }

}
