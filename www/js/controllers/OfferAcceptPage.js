class OfferAcceptPage {
    constructor(uid, appObject, userModel) {
        var that = this;
        this._app = appObject;
        this._userModel = userModel;
        this._uid = uid;

        this.initUI();
    }


    initUI() {
        this.loadUserCards()
        var that = this;
        $$("#select-btn").click(function () {
            that.saveSelection(that);
        })
    }

    saveSelection(that) {
        if (!that._adapter) {
            that._app.dialog.alert("Данныет еще не были загружены", "Ошибка!");
            return;
        }
        if (that._adapter.getSelectedItems().length == 0) {
            that._app.dialog.alert("Вы не отметили ни одну фишку    ", "Ошибка!");
            return;
        }
        that._app.preloader.show();
        var onSuccess = function (data) {
            that._app.dialog.alert(data.text, "Выполнено!");
            that._app.preloader.hide();
            that.loadUserCards();
        }

        var onError = function (error) {
            console.log(error);
            that._app.dialog.alert(error.responseText, "Ошибка!");
            that._app.preloader.hide();
        }

        var data = {
            userId: that._uid,
            items: that._adapter.getSelectedItems()
        }

        this._userModel.sendStamps(data, onSuccess, onError);
    }

    showUserCards(that, data) {
        $$("#user-name").text("ID" + data.user.id + " / " + data.user.name);
        that._adapter = new SalepointUserCardsAdapter(that._app, $$("#cards"));
        that._adapter.clearList();
        that._adapter.setData(data.items);
        that._adapter.render();
    }

    loadUserCards() {
        var that = this;
        that._app.preloader.show();
        var onSuccess = function (data) {
            that.showUserCards(that, data);
            that._app.preloader.hide();
        }
        var onFailure = function (data) {
            that._app.preloader.hide();
            that._app.dialog.alert(data.responseText, "Ошибка!", function () {
                that._app.views.main.router.back();
            });

        }
        this._userModel.loadUserCards({userId: this._uid}, onSuccess, onFailure);
    }

}