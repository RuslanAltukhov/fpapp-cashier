class LoginPage {
    constructor(config, appObject, userModel) {
        this._loginForm = $$('#login_form');
        this._app = appObject;
        this._userModel = userModel;
        var that = this;
        $$('#login_btn').click(function () {
            that.loginUser();
        })
    }

    loginUser() {
        var formData = app.form.convertToData(this._loginForm);
        var that = this;
        var useLoginSuccess = function (data) {
            console.log(data);
            that._app.views.main.router.navigate("/profile/");
            User.saveToStorage(data.cashier);
            authToken = AuthToken.createNew(data.token.token);
            authToken.save();
            onAuthorized(authToken);

        }
        var userLoginFailure = function (data) {
            that._app.dialog.alert(data.responseText, 'Ошибка');
        }
        this._userModel.loginUser(formData, useLoginSuccess, userLoginFailure);

    }

    shown() {

    }
}