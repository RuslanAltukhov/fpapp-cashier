class SupportPageController {
    constructor(app, userModel) {
        this._app = app;
        this._userModel = userModel;
        this.initUI();
    }

    initUI() {
        var that = this;
        this._message = $$("#message");
        this._sendMsgBtn = $$("#send_msg_btn");
        this._sendMsgBtn.click(function () {
            that.sendMessage();
        })
    }

    sendMessage() {
        var that = this;
        var onSucces = function (data) {
            that._message.val("");
            that._app.dialog.alert(data.text, "Выполнено");
        }
        var onError = function (data) {
            that._app.dialog.alert("Сообщение не отправлено", "Ошибка");
        }
        this._userModel.sendMessage({message: this._message.val()}, onSucces, onError);
    }

}