var config = {
    domain: 'https://app.comeback.plus',
    urlList: {
        login: '/api/cashier/login',
        history: '/api/cashier/sales-history',
        checkAuthToken: '/api/cashier/check-token',
        userCards: '/api/cashier/user-cards',
        supportMessage: '/api/cashier/send-message',
        salepoints: '/api/cashier/salepoints',
        stamps: '/api/cashier/check-items',
        chooseSalepoint: '/api/cashier/set-salepoint'
    },
    getFullUrl: function (path) {
        return this.domain + path;
    },
    getFullUrlWithSessid: function (path, sessid) {
        return this.domain + path + "?sessid=" + sessid;
    }
}