// Initialize app
var $$ = Dom7;
var app = new Framework7({
    root: '#app',
    panel: {
        swipe: 'left',

    },
    popup: {
        backdrop: false,
        swipeToClose: true,
    },
    touch: {
        // Enable fast clicks
        fastClicks: true,
    },
    navbar: {
        hideOnPageScroll: true,
        iosCenterTitle: true,
    },
    routes: [
        {
            name: "about",
            path: '/about/',
            url: 'about.html',
        },
        {
            name: "login",
            path: '/login/',
            url: 'login.html',
        },
        {
            name: "register",
            path: '/register/',
            url: 'register.html',
        },
        {
            name: "all_actions",
            path: '/all-actions/',
            url: 'all_actions.html',
        },
        {
            name: "my-actions",
            path: '/my-actions/',
            url: 'my-actions.html',
        },
        {
            name: "profile",
            path: '/profile/',
            url: 'profile.html',
        }
    ]
});
window.onerror = function (e) {
    app.dialog.alert(e);
};
var loginPage;
var allActionsPage;
var userActionsPage;
// If we need to use custom DOM library, let's save it to $$ variable:


// Add view
var mainView = app.views.create('.view-main', {
    // Because we want to use dynamic navbar, we need to enable it for this view:
    dynamicNavbar: true,
    url: '/login/'
});
$$('.left-menu a').on('click', function () {
    app.panel.close();
});
// Handle Cordova Device Ready Event
$$(document).on('deviceready', function () {
    /*  app.preloader.show();
      var authToken = AuthToken.getFromStorage(config, app);
      if (!authToken) {
          console.log('auth token is not exist');
          return;
      }
      var onSuccess = function (data) {
          app.views.main.router.navigate("/all-actions/");
          app.preloader.hide();
      }
      var onFailure = function (data) {
          app.preloader.hide();
      }
      authToken.isActive(onSuccess, onFailure);*/

});
// Now we need to run the code that will be executed only for About page.


// Option 2. Using one 'pageInit' event handler for all pages:
$$(document).on('page:init', '.page[data-name="login"]', function (e) {
    loginPage = new LoginPage(config, app, new User(app, config));
});
$$(document).on('page:afterin', '.page[data-name="all_actions"]', function (e) {
    app.panel.disableSwipe("left");
});
$$(document).on('page:afterout', '.page[data-name="all_actions"]', function (e) {
    app.panel.enableSwipe("left");
});
$$(document).on('page:init', '.page[data-name="all_actions"]', function (e) {

    allActionsPage = new AllActionsPage(config, app, new User(app, config));
    allActionsPage.showActions();

});
$$(document).on('page:afterin', '.page[data-name="my_actions"]', function (e) {
    userActionsPage = new UserActionsPage(config, app, new User(app, config));
    userActionsPage.showActions();
});