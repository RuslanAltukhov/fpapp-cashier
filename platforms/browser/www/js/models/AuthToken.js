class AuthToken {

    constructor(jsoData, config, appObject) {
        if (!jsoData.token) {
            throw new Error("'token' param is expected but not exist");
        }
        this._config = config;
        this._app = appObject;
        this._token = jsoData.token;
    }

    static createNew(token, config, appObject) {
        return new AuthToken({token: token}, config, appObject);
    }

    getAsJson() {
        return {token: this._token};
    }

    static getLocalStorageKey() {
        return 'authToken';
    }

    save() {
        var storage = window.localStorage;
        storage.setItem(AuthToken.getLocalStorageKey(), JSON.stringify(this.getAsJson()));
    }

    static getFromStorage(config, appObject) {
        var storage = window.localStorage;
        var data = storage.getItem(AuthToken.getLocalStorageKey());

        return data ? new AuthToken(JSON.parse(data), config, appObject) : null;
    }

    isActive(onSuccess, onFailure) {
        console.log(this._config.getFullUrl(this._config.urlList.checkAuthToken));
        this._app.request.post(this._config.getFullUrl(this._config.urlList.checkAuthToken), {sessid: this._token}, function (data) {
            if (data.code == 0) { // login success
                onSuccess(data);
                return;
            }
            onFailure(data);
        }, null, 'json');
    }
}