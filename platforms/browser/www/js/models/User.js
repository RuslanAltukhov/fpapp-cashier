class User {
    constructor(appObject, config) {
        this._appObject = appObject;
        this._config = config;
    }

    loginUser(loginAndPassword, onSuccess, onFailure) {
        this._appObject.request.post(this._config.getFullUrl(this._config.urlList.login), loginAndPassword, function (data) {
            if (data.code == 0) { // login success
                onSuccess(data);
                return;
            }

            onFailure(data);
        }, null, 'json');
    }
}