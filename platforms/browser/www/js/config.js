var config = {
    domain: 'http://fpapp.loc',
    urlList: {
        login: '/api/users/login',
        registration: '/api/users/create-user',
        checkAuthToken: '/api/users/check-token',
    },
    getFullUrl: function (path) {
        return this.domain + path;
    }
}