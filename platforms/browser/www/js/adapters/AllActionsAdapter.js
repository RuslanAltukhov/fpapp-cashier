class AllActionsAdapter {
    constructor(app, container) {
        this._container = container;
        this._app = app;
        this.setData([]);
    }

    static getTemplate() {
        return '<div class="card">\n' +
            '            <div class="card-header">{0}</div>\n' +
            '            <div class="card-content card-content-padding">{1}</div>\n' +
            '            <div class="card-footer">{2}</div>\n' +
            '        </div>';
    }

    setData(data) {
        this._data = data;
        return this;
    }

    addItems(data) {
        this._data = this._data.concat(data);
        return this;
    }

    addContentToItem(el, data) {
        $$(el).find('.card-header').text(data.title);
        return el;
    }

    render() {
        var template = AllActionsAdapter.getTemplate();
        var that = this;
        this._data.forEach(function (item) {
            var el = that.addContentToItem($$(template), item);
            $$(that._container).append(el);
        });
    }
}