class AllActionsPage {
    constructor(config, appObject, userModel) {
        this._app = appObject;
        this._userModel = userModel;
        this._actionsModel = new Actions(this._app, config);
        var that = this;
        $$('#actions_on_map').on("tab:show", function () {
            that.initActionsMap();
        });
    }

    showActions() {

        var that = this;
        var actionsLoaded = function (data) {
            var actionsListAdapter = new AllActionsAdapter(that._app, $$("#all_actions_container"));
            actionsListAdapter.setData(data);
            actionsListAdapter.render();
        }
        this._actionsModel.loadAllActions({}, actionsLoaded, null);
    }

    getPageHeight() {
        return $$('.page-content').height();
    }

    initActionsMap() {

        $$("#map").css({'height': this.getPageHeight() + 'px'});
        var iconFeatures = [];

        var iconFeature = new ol.Feature({
            geometry: new ol.geom.Point(ol.proj.transform([-72.0704, 46.678], 'EPSG:4326',
                'EPSG:3857')),
            name: 'Null Island',
            population: 4000,
            rainfall: 500
        });

        var iconFeature1 = new ol.Feature({
            geometry: new ol.geom.Point(ol.proj.transform([-73.1234, 45.678], 'EPSG:4326',
                'EPSG:3857')),
            name: 'Null Island Two',
            population: 4001,
            rainfall: 501
        });

        iconFeatures.push(iconFeature);
        iconFeatures.push(iconFeature1);

        var vectorSource = new ol.source.Vector({
            features: iconFeatures //add an array of features
        });

        var iconStyle = new ol.style.Style({
            image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
                anchor: [0.5, 46],
                anchorXUnits: 'pixels',
                anchorYUnits: 'pixels',
                opacity: 1,
                src: 'img/marker.png'
            }))
        });


        var vectorLayer = new ol.layer.Vector({
            source: vectorSource,
            style: iconStyle
        });
        var interactions = ol.interaction.defaults({altShiftDragRotate: false, pinchRotate: false});
        var map = new ol.Map({
            interactions: interactions,
            target: 'map',
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM()
                }),
                vectorLayer
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([-73.1234, 45.678]),
                zoom: 10
            })
        });
        var that = this;
        map.on("movestart", function (e) {
            console.log("movestart");
            e.stopPropagation();
            e.preventDefault();
        });
        map.on("moveend", function (e) {
            e.stopPropagation();
            e.preventDefault();
        });
        map.on('click', function (evt) {
            var feature = map.forEachFeatureAtPixel(evt.pixel,
                function (feature, layer) {
                    return feature;
                });
            if (feature) {
                // Create dynamic Popover
                var dynamicPopover = that._app.popover.create({
                    targetX: evt.pixel[0],
                    targetY: evt.pixel[1],
                    content: '<div class="popover company-on-map">' +
                    '<div class="popover-inner">' +
                    '<div class="block map-item">' +
                    '<a href="#" class="link color-white popover-close"><i class="material-icons">close</i></a>' +
                    '<div class="map-item--logo">' +
                    '<img src="../img/temp/burger.png">' +
                    '</div>' +
                    '<div class="map-item--info">' +
                    '<div class="map-item--title">Название компании</div>' +
                    '<div class="map-item--adress">Адрес компании</div>' +
                    '<div class="map-item--description">Здесь примечание, которое компания в админке написала о совей ТТ</div>' +
                    '</div>' +
                    '<div class="map-item--footer"><button class="button button-small">Все предложения</button></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>',
                    // Events
                    on: {
                        open: function (popover) {
                            console.log('Popover open');
                        },
                        opened: function (popover) {
                            console.log('Popover opened');
                        },
                    }
                });
                dynamicPopover.open();
            }
        });
    }
}