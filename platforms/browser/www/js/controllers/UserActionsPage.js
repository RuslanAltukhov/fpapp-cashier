class UserActionsPage {
    constructor(config, appObject) {
        this._app = appObject;
        this._actionsModel = new Actions(this._app, config);
    }

    showActions() {
        var that = this;
        var actionsLoaded = function (data) {
            var actionsListAdapter = new AllActionsAdapter(that._app, $$("#actions_container"));
            actionsListAdapter.setData(data);
            actionsListAdapter.render();
        }
      //  this._actionsModel.loadAllActions({}, actionsLoaded, null);
    }
}